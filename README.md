# Express Starter

This is a starter project for creating a simple web server using Express with Node.js. This project sets up a basic server that responds with "Hello World!" when you visit the root URL.

## Prerequisites

- [Node.js](https://nodejs.org/) (v12 or higher)
- [npm](https://www.npmjs.com/) (v6 or higher)

## Installation

**Clone the repository:**

```bash
git clone https://gitlab.com/ewilan-riviere/express-starter.git
```

**Navigate to the project directory:**

```bash
cd express-starter
```

**Install the dependencies:**

```bash
npm install
```

## Running the Application

**Start the server:**

```bash
npm run start
```

**Open your web browser and visit: <http://localhost:3000>**

You should see the message "Hello World!".

## Development

**Start the server with nodemon:**

```bash
npm run dev
```

This will start the server using [nodemon](https://nodemon.io/), which will automatically restart the server whenever you make changes to the code.
